using UnityEngine;

//Script que gestiona el movimiento automatico del "jugador" en el nivel 2 (en el que disparas)
public class PlayerAutomaticMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    private int _rayDirection = 1;

    private Animator _animator;
    private SpriteRenderer _sprite;

    private RaycastHit2D _ray;
    private bool _rayIsTouching;

    private void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        Physics2D.queriesStartInColliders = false; //Ignora su propio collider para que el ray no coja su propia info
    }

    private void Update()
    {
        PlayerAutoMove();
        _ray = Physics2D.Raycast(this.transform.position, new Vector3(_rayDirection, -1.5f, 0));
        Debug.DrawRay(transform.position, new Vector3(_rayDirection, -1.5f, 0), Color.red);

        if (_ray.collider.CompareTag("Untagged")) _rayIsTouching = false;
        else if (_ray.collider.CompareTag("Ground")) _rayIsTouching = true;
    }

    private void PlayerAutoMove()
    {
        if (!_rayIsTouching)
        {
            _rayDirection *= -1;
            _speed *= -1;

            if (_sprite.flipX == false) _sprite.flipX = true; else _sprite.flipX = false;
        }

        transform.position += new Vector3(_speed * Time.deltaTime, 0, 0);

        _animator.SetFloat("PlayerSpeed", Mathf.Abs(_speed));
    }

    public void DieByFireBall() { _animator.SetBool("DefeatedByFireBall", true); }
    public void DieByThunder(){ _animator.SetBool("DefeatedByThunder", true); }
}
