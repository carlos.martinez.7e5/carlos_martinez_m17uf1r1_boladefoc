using UnityEngine;
using UnityEngine.SceneManagement;

//Script que gestiona si morir o mantenerse vivo significa ganar o perder, ya que dependiendo del nivel
//los objetivos son opuestos.
public class LoseOrWin : MonoBehaviour
{
    Scene currentScene;
    private void Start(){ currentScene = SceneManager.GetActiveScene(); }

    public void PlayerDie()
    {
        if (currentScene.name == "Lvl1Scene") SceneManagement.ToLose();
        else if (currentScene.name == "Lvl2Scene") SceneManagement.ToWin();
    }

    public void PlayerStay()
    {
        if (currentScene.name == "Lvl1Scene") SceneManagement.ToWin();
        else if (currentScene.name == "Lvl2Scene") SceneManagement.ToLose();
    }
}
