using UnityEngine;

//Script que gestiona el movimiento del jugador (manual) del nivel 1
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    private Animator _animator;

    public bool lockMovementOnWin;
    public bool _canJump;

    void Awake() { _animator = GetComponent<Animator>(); }

    void Update()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            if(Input.GetAxis("Horizontal") > 0) GetComponent<SpriteRenderer>().flipX = true;
            else GetComponent<SpriteRenderer>().flipX = false;
            
            ManualMovement();
            _animator.SetFloat("PlayerSpeed", Mathf.Abs(Input.GetAxis("Horizontal")));
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.UpArrow)) Jump();
    }
   
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            _canJump = true;
            _animator.SetBool("IsJumping", false);
        }    
    }

    private void OnCollisionStay2D(Collision2D other) { if (other.collider.CompareTag("Ground")) _canJump = true; }

    private void OnCollisionExit2D(Collision2D collision) { _canJump = false; }

    public void ManualMovement(){ transform.position += new Vector3(Input.GetAxis("Horizontal") * Time.deltaTime * _speed, 0, 0); }

    public void Jump()
    {
        if (_canJump)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
            _animator.SetBool("IsJumping", true);
        }
    }

    public void DieByFireBall() { _animator.SetBool("DefeatedByFireBall", true); }

    public void DieByThunder() { _animator.SetBool("DefeatedByThunder", true); }
}
