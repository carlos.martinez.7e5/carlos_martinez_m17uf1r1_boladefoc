using UnityEngine;

//Script que gestiona el poder (o desventaja) de gegantina (al tocar el veneno)
public class GegantinaPower : MonoBehaviour
{

    [SerializeField] private float _timeBeingGiant;
    private float _giantCooldown;
    [SerializeField] private float _sizeMultiplier;
    [SerializeField] private float _growingSpeed;
    [SerializeField] public States _grow;

    private GameObject _player;
    private float _originalSize;

    private void Start()
    {
        _player = this.gameObject;
        _originalSize = _player.transform.localScale.x; 
    }

    public enum States
    {
        nothing,
        growing,
        beingGiant,
        decreasing
    }

    void Update()
    {
        if (_sizeMultiplier < 1) _sizeMultiplier = 1;

        switch (_grow)
        {
            case States.growing:
                GrowUp();
                break;
            case States.decreasing:
                GrowDown();
                break;
            case States.beingGiant:
                BeGiant();
                break;
            default:
                break;
        }
    }

    private void GrowUp()
    {
        if (_player.transform.localScale.x < _originalSize * _sizeMultiplier)
        {
          
            _player.transform.localScale += new Vector3((float)_growingSpeed * Time.deltaTime,
                (float)_growingSpeed * Time.deltaTime,
                0);
        }
        else
        {
            _grow = States.beingGiant;
            _giantCooldown = _timeBeingGiant;
        }
    }

    private void GrowDown()
    {
        if (_player.transform.localScale.x > _originalSize)
        {
            _player.transform.localScale += new Vector3(-(float)_growingSpeed * Time.deltaTime,
                -(float)_growingSpeed * Time.deltaTime,
                0);
        }
        else _grow = States.nothing;
    }

    private void BeGiant()
    {
        if (_giantCooldown <= 0) _grow = States.decreasing;
        else _giantCooldown -= Time.deltaTime;
    }
}
