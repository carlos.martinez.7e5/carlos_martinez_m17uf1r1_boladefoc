using UnityEngine;
using UnityEngine.UI;

//Script que gestiona los controles con el rat�n del nivel 2
public class MouseControls : MonoBehaviour
{ 
    [SerializeField] Camera camara;
    
    [SerializeField] float timeBetweenFireBalls;
    float fireBallCooldown;

    [SerializeField] float timeBetweenThunders;
    float thunderCooldown;

    private Animator _wizardAnim;

    [Header ("Player Killers")]
    [SerializeField] GameObject fireball;
    [SerializeField] GameObject warning;
    [SerializeField] GameObject poison;
    [SerializeField] int _poisonChance;

    private void Start()
    {
        fireBallCooldown = 0;
        thunderCooldown = 0;
        _wizardAnim = GameObject.Find("EvilWizard").GetComponent<Animator>();
    }

    void Update()
    {
        if (fireBallCooldown <= 0)
        {
            GameObject.Find("fireball").GetComponent<Image>().color = Color.white;

            if (Input.GetMouseButtonDown(0))
            { 
                var position = camara.ScreenToWorldPoint(Input.mousePosition);
                
                // position[0]; X
                position[1] = 5; //Y
                position[2] = 0; //Z

                int poisonAppear = Random.Range(0, (_poisonChance + 1));

                if (poisonAppear != 7) Instantiate(fireball, position, new Quaternion(0f, 0f, 0f, 0f));
                else Instantiate(poison, position, Quaternion.identity);

                GameObject.Find("fireball").GetComponent<Image>().color = Color.red;

                fireBallCooldown = timeBetweenFireBalls;

                _wizardAnim.SetTrigger("Attack");

            }
        }
        else fireBallCooldown -= Time.deltaTime;

        if (thunderCooldown <= 0)
        {
            GameObject.Find("thunder").GetComponent<Image>().color = Color.white;

            if (Input.GetMouseButtonDown(1))
            {
                GameObject.Find("thunder").GetComponent<Image>().color = Color.red;

                var position = camara.ScreenToWorldPoint(Input.mousePosition);
                position[1] = -2;
                position[2] = 0;
                Instantiate(warning, position, new Quaternion(0f, 0f, 0f, 0f));
                thunderCooldown = timeBetweenThunders;
            }
        }
        else thunderCooldown -= Time.deltaTime;
    }
}
