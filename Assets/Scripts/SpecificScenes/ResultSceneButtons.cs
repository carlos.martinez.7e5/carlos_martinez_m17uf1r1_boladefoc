using UnityEngine;
using UnityEngine.UI;

//Scritp que gestiona las pantallas de derrota y victoria. Para saber a cual ir dependiendo de cual venga
public class ResultSceneButtons : MonoBehaviour
{
    [SerializeField] private Button _playAgain;
    [SerializeField] private Button _backToMenu;
    
    private void Start()
    {
        var sceneDetails = GameObject.Find("SceneDetails").GetComponent<SceneDetails>();

        if (sceneDetails.previousScene == "Lvl1Scene") _playAgain.onClick.AddListener(SceneManagement.ToLvl1);
        else if (sceneDetails.previousScene == "Lvl2Scene") _playAgain.onClick.AddListener(SceneManagement.ToLvl2);

        _backToMenu.onClick.AddListener(SceneManagement.ToMenu);
    }
}
