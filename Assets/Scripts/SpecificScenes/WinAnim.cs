using UnityEngine;

//Script que gestiona la animación de ganar y la desactiva para que no quede en bucle
public class WinAnim : MonoBehaviour  
{
    private Animator _playerAnimator;
    
    void Start()
    {
        _playerAnimator = GameObject.Find("Player").GetComponent<Animator>();
        _playerAnimator.SetBool("Win", true);
    }

    void SetFalse() { _playerAnimator.SetBool("Win", false); }
}
