using UnityEngine;
using UnityEngine.SceneManagement;

//Script que guarda y gestiona los clips de audio seg�n la escena
public class MusicController : MonoBehaviour
{
    public AudioClip[] audioClips = new AudioClip[5];
    
    [SerializeField] private AudioSource _audioSource;
    private Scene _currentScene;

    private void Awake() { SceneManager.sceneLoaded += OnSceneLoaded; }
    
    void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        _currentScene = SceneManager.GetActiveScene();

        switch (_currentScene.name)
        {
            case "MainMenuScene":
                _audioSource.clip = audioClips[0];
                _audioSource.Play();
                break;
            case "Lvl1Scene":
                _audioSource.clip = audioClips[1];
                _audioSource.Play();
                break;
            case "Lvl2Scene":
                _audioSource.clip = audioClips[2];
                _audioSource.Play();
                break;
            case "WinScene":
                _audioSource.clip = audioClips[3];
               _audioSource.Play();
                break;
            case "LoseScene":
                _audioSource.clip = audioClips[4];
                _audioSource.Play();
                break;
        }
    }
}
