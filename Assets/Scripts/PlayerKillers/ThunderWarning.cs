using UnityEngine;

//Script que define como se comportan los thunder warning
public class ThunderWarning : MonoBehaviour
{
    [SerializeField] private float _lifetime = 4;
    [SerializeField] GameObject thunder;

    private void Update()
    {
        _lifetime -= Time.deltaTime;
        if (_lifetime <= 0) Destroy(this.gameObject);
    }

    //El instantiatie tiene que estar aqui dentro y no en el spawnerManager para poder llamarlo desde un evento en la animacion
    //del aviso, y no tener que ponerle como componente todo el manager.
    public void InstantiateThunder()
    {
        Vector3 thunderPosition = new Vector3(transform.position.x, (transform.position.y + 1.5f), transform.position.z);
        Instantiate(thunder, thunderPosition, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
