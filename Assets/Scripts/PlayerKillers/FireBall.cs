using UnityEngine;

//Script que define como se comportan las fireballs
public class FireBall : MonoBehaviour
{
    [SerializeField] private Animator _animator; 

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            if(other.gameObject.GetComponent<PlayerMovement>()) other.gameObject.GetComponent<PlayerMovement>().DieByFireBall();
            else other.gameObject.GetComponent<PlayerAutomaticMovement>().DieByFireBall();
            
            Destroy(this.gameObject);
        }
        else if (other.collider.CompareTag("Ground")) _animator.SetBool("TouchedGround", true);
        else if (!other.collider.CompareTag("FireBall")) Destroy(this.gameObject);
    }

    private void DestroySelf() { Destroy(this.gameObject); }
}
