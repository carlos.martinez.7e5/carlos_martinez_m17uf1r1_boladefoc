using UnityEngine;

//Script que define como se comporta la DeathZone (caerse por los bordes)
public class DeathZone : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other) { if (other.collider.CompareTag("Player")) { SceneManagement.ToLose(); } }
}
