using UnityEngine;

//Script que define como se comportan los thunder
public class Thunder : MonoBehaviour
{
    [SerializeField] private float _lifetime = 5;

    private void Update()
    {
        _lifetime -= Time.deltaTime;
        if (_lifetime <= 0) Destroy(this.gameObject); //Hacer que avise antes de caer
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<PlayerMovement>()) other.gameObject.GetComponent<PlayerMovement>().DieByThunder();
            else other.gameObject.GetComponent<PlayerAutomaticMovement>().DieByThunder(); 
        }
    }
}
