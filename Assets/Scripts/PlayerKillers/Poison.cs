using UnityEngine;

//Script que define como se comportan las "poison mushrooms"
public class Poison : MonoBehaviour
{
    [SerializeField] private float _lifeTime;
    private float _timeToDie;

    private void Start() {_timeToDie = _lifeTime; }

    private void Update()
    {
        _lifeTime -= Time.deltaTime;
        if (_lifeTime <= 0) Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Player"))
        {
            var gegantina = other.gameObject.GetComponent<GegantinaPower>();
            
            //Si por lo que sea puede cojer dos (muy improbable) no se suman los efectos
            if(gegantina._grow == GegantinaPower.States.nothing) gegantina._grow = GegantinaPower.States.growing;
            Destroy(this.gameObject);
        }

        if (other.collider.CompareTag("Thunder")) Destroy(other.gameObject);
    }
}
