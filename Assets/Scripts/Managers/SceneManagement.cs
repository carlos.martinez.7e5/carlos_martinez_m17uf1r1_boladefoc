using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Script que gestiona los cambios entre escenas
public class SceneManagement : MonoBehaviour
{
    public static void ToMenu() { SceneManager.LoadScene("MainMenuScene"); }

    public static void ToLvl1() { 
        
        if (SceneManager.GetActiveScene().name == "MainMenuScene")
        {
            var inputField = GameObject.Find("inputF_PlayerName").GetComponent<InputField>();
            //Verifica que el input text del nombre no est� vac�o
            if (inputField.text != "")
            {
                GameObject.Find("PlayerNameHolder").GetComponent<NameHolder>().PlayerName = inputField.text;
                SceneManager.LoadScene("Lvl1Scene");
            }
        } 
        else SceneManager.LoadScene("Lvl1Scene"); 
    }

    public static void ToLvl2() {
        
        if (SceneManager.GetActiveScene().name == "MainMenuScene")
        {
            var inputField = GameObject.Find("inputF_PlayerName").GetComponent<InputField>();
            if (inputField.text != "")
            {
                GameObject.Find("PlayerNameHolder").GetComponent<NameHolder>().PlayerName = inputField.text;
                SceneManager.LoadScene("Lvl2Scene");
            }
        }
        else SceneManager.LoadScene("Lvl2Scene");
    }

    public static void ToWin() { SceneManager.LoadScene("WinScene"); }

    public static void ToLose() { SceneManager.LoadScene("LoseScene"); }

    public static void Exit() { Application.Quit(); }  
}
