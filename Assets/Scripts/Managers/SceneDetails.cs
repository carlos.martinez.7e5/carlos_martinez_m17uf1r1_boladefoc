using UnityEngine;
using UnityEngine.SceneManagement;

//Script que se encarga de guardar la escena actual y la anterior. Es necesaria para las pantallas de derrota y victoria,
//hace falta saber de que escena se viene para volver a ella.
public class SceneDetails : MonoBehaviour
{
    public string previousScene;
    public string currentScene;

    private void Awake()
    {
        currentScene = SceneManager.GetActiveScene().name;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        previousScene = currentScene;
        currentScene = SceneManager.GetActiveScene().name;
    }
}
