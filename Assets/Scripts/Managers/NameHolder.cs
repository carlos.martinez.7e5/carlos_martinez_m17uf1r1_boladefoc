using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

//Script que se encarga de guardar el nombre del jugador entre escenas
public class NameHolder : MonoBehaviour
{
    public string PlayerName;
    private Scene _currentScene;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        _currentScene = SceneManager.GetActiveScene();

        switch (_currentScene.name)
        {
            case "Lvl1Scene":
            case "Lvl2Scene":
                GameObject.Find("PlayerName").GetComponent<TextMeshPro>().text = PlayerName;
                break;
            default:
                break;
        }
    }
}
