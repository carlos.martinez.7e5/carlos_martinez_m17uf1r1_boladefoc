using UnityEngine;
using UnityEngine.UI;

//Script que gestiona el spawner de los playerKillers (fireball, thunder y poison)
public class SpawnerManager : MonoBehaviour
{
    public GameObject fireball; //prefab de fireball
    public GameObject thunder; //prefab de thunder
    public GameObject thunderWarning; //prefab del aviso de que thunder caer�
    public GameObject poison; //prefab de poison

    [SerializeField] private float _secondsBetweenFireballs; //Cada cuanto tiempo caer� una fireball
    [SerializeField] private float _secondsBetweenThunders; //Cada cuanto tiempro caer� un thunder
    [SerializeField] private float _thunderWarningTime; //Cuanto durar� el aviso del thunder
   
    private float _fireballRefreshTime; //Variable usada para comprobar cuanto tiempo le falta para intanciar otra fireball
    private float _thunderRefreshTime = 3; //Variable usada para comprobar cuanto tiempo le falta para intanciar otro thunder
                                           //Comienza a 3 para que no tire uno autom�ticamente 
    
    [SerializeField] private int _poisonChance; //Probabilidad de spawnear poison en vezd de fireball

    private Text text;

    private bool fase2 = false; //Seg�n la fase la cantidad de fireballs aumenta
    private bool fase3 = false;

    private Animator _wizardAnim;

    private void Start()
    {
        if (_poisonChance < 10) _poisonChance = 10; //Para que siempre sea minimo 10
        text = GameObject.Find("txt_timer").GetComponent<Text>();
        _wizardAnim = GameObject.Find("EvilWizard").GetComponent<Animator>();
    }

    void Update()
    {
        if (int.Parse(text.text) <= 40 && !fase2)
        {
            _secondsBetweenFireballs /= 2;
            fase2 = true;
        }

        if (int.Parse(text.text) <= 20 && !fase3)
        {
            _secondsBetweenFireballs /= 2;
            fase3 = true;
        }

        //Spawnea fireballs / poison
        if (_fireballRefreshTime <= 0) SpawnFireBall();
        else _fireballRefreshTime -= Time.deltaTime;

        //Spawnea thunders
        if (_thunderRefreshTime <= 0) SpawnThunderWarning(); //SpawnThunder();         
        else _thunderRefreshTime -= Time.deltaTime;
    }

    private void SpawnFireBall()
    {
        float randomX = Random.Range(-7f, 7f);
        int poisonAppear = Random.Range(0, (_poisonChance + 1));
        
        Vector3 fireBallPosition = new Vector3(randomX,
        this.transform.position.y, this.transform.position.z);

        //7 Numero aleatorio, puede ser cualquiera, el punto es que es 1 posibilidad entre X (poisonChance)
        if (poisonAppear != 7) Instantiate(fireball, fireBallPosition, new Quaternion(0f, 0f, 0f, 0f));
        else Instantiate(poison, fireBallPosition, Quaternion.identity);

        _fireballRefreshTime = _secondsBetweenFireballs;

        _wizardAnim.SetTrigger("Attack");
    }

    private void SpawnThunderWarning()
    {
        float randomX = Random.Range(-7f, 7f);

        Vector3 warningPosition = new Vector3(randomX,
           -2f, this.transform.position.z);

        Instantiate(thunderWarning, warningPosition, Quaternion.identity);

        _thunderRefreshTime = _secondsBetweenThunders;

        _wizardAnim.SetTrigger("Attack");
    }

    private void SpawnThunder()
    {
        Vector3 thunderPosition = new Vector3(transform.position.x, (transform.position.y + 1.5f), transform.position.z);
        Instantiate(thunder, thunderPosition, Quaternion.identity);
        Destroy(this.gameObject);
    }
}
