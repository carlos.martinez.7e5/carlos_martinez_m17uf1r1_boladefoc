using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Script que gestiona el contador de tiempo
public class TimerManager : MonoBehaviour
{
    [SerializeField] private double _seconds;
    [SerializeField] private GameObject _timeTxt;
    [SerializeField] private Animator _animator;

    void Update()
    {
        _seconds -= Time.deltaTime;
        _timeTxt.GetComponent<Text>().text = Mathf.Round((float)_seconds).ToString();

        if (_seconds < 0) {
            Scene _currentScene = SceneManager.GetActiveScene();

            //Como las condiciones de victoria entre los dos niveles es contraria, tiene que revisar en que
            //escena est� para saber si que se agote el tiempo significa ganar o perder
            if (_currentScene.name == "Lvl1Scene") SceneManagement.ToWin();
            else SceneManagement.ToLose();
        }
    }
}
